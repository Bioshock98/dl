package ru.pyshinskiy;

import ru.pyshinskiy.bootstrap.Bootstrap;
import ru.pyshinskiy.listener.DirectoryListener;

import java.io.File;

public class Main {

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap(args[0]);
        bootstrap.processDirectory();
    }
}
